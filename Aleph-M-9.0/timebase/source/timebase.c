#include "../timebase.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>

static struct {
	timebase_handler_ft *handler_isr;
	void *handler_arg;
} timebase = { 0 };

void  timebase_start(timebase_handler_ft *handler, void *arg)
{
	/* konfiguracja częstotliwości 1kHz */
	static uint8_t const PRESCALER_32 = 3U;
	static uint8_t const TOP = 249U;

	timebase.handler_isr = handler;
	timebase.handler_arg = arg;

	TCCR2 = (uint8_t)((1U << WGM21) + PRESCALER_32);
	TIFR |= (1U << OCF2); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIMSK |= (1U << OCIE2);
	OCR2 = TOP;
	TCNT2 = 0U;
}

ISR(TIMER2_COMP_vect)
{
	(timebase.handler_isr)(timebase.handler_arg);
}

