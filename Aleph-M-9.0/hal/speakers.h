#pragma once

/** \file **********************************************************************
 *
 * sterowanie podłączaniem zespołów głośnikowych do końcówki mocy
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjalizacja sterowania głośnikami
 */
void hal_speakers_init(void);

/*
 * podłącza głośniki
 */
void hal_speakers_on(void);

/*
 * odłącza głośniki
 */
void hal_speakers_off(void);

/*
 * sprawdza czy głośniki są podłączone
 *
 * @ret		true - podłączone
 *		false - niepodłączone
 */
bool hal_speakers_is_on(void);

