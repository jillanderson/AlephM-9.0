#include "../speakers.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <stdbool.h>

static avr_gpio_pin_st const RELAY_SPEAKER_L = (avr_gpio_pin_st ) { .port = &PORTC, .pin_number = PC4 };
static avr_gpio_pin_st const RELAY_SPEAKER_R = (avr_gpio_pin_st ) { .port = &PORTC, .pin_number = PC5 };

static bool speaker_left_is_on(void);
static bool speaker_right_is_on(void);

void hal_speakers_init(void)
{
	avr_gpio_configure_as_output(&RELAY_SPEAKER_L);
	avr_gpio_configure_as_output(&RELAY_SPEAKER_R);
	avr_gpio_set_low(&RELAY_SPEAKER_L);
	avr_gpio_set_low(&RELAY_SPEAKER_R);
}

void hal_speakers_on(void)
{
	avr_gpio_set_high(&RELAY_SPEAKER_L);
	avr_gpio_set_high(&RELAY_SPEAKER_R);
}

void hal_speakers_off(void)
{
	avr_gpio_set_low(&RELAY_SPEAKER_L);
	avr_gpio_set_low(&RELAY_SPEAKER_R);
}

bool hal_speakers_is_on(void)
{
	return (((true == speaker_left_is_on())
			&& (true == speaker_right_is_on())) ? true : false);
}

bool speaker_left_is_on(void)
{
	return (0U == avr_gpio_read_pin(&RELAY_SPEAKER_L) ? false : true);
}

bool speaker_right_is_on(void)
{
	return (0U == avr_gpio_read_pin(&RELAY_SPEAKER_R) ? false : true);
}
