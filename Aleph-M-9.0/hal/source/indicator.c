#include "../indicator.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <stdbool.h>

static avr_gpio_pin_st const LED_INDICATOR = (avr_gpio_pin_st ) { .port = &PORTB, .pin_number = PB2 };

void hal_indicator_init(void)
{
	avr_gpio_configure_as_output(&LED_INDICATOR);
	avr_gpio_set_low(&LED_INDICATOR);
}

void hal_indicator_on(void)
{
	avr_gpio_set_high(&LED_INDICATOR);
}

void hal_indicator_off(void)
{
	avr_gpio_set_low(&LED_INDICATOR);
}

void hal_indicator_toggle(void)
{
	avr_gpio_set_opposite(&LED_INDICATOR);
}

bool hal_indicator_is_on(void)
{
	return (0U == avr_gpio_read_pin(&LED_INDICATOR) ? false : true);
}

