#include "../key.h"

#include <avr/avr_gpio.h>
#include <avr/io.h>
#include <stdbool.h>

static avr_gpio_pin_st const KEY = (avr_gpio_pin_st ) { .port = &PORTB, .pin_number = PB1 };

void hal_key_init(void)
{
	avr_gpio_configure_as_input(&KEY);
	avr_gpio_set_low(&KEY);
}

bool hal_key_is_pressed(void)
{
	return (0U == avr_gpio_read_pin(&KEY) ? true : false);
}

