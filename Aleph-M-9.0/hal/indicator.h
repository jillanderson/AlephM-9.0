#pragma once

/** \file **********************************************************************
 *
 * sterowanie diodą led w przycisku zasilania
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje wskaźnik optyczny
 */
void hal_indicator_init(void);

/*
 * włącza wskaźnik optyczny
 */
void hal_indicator_on(void);

/*
 * wyłącza wskaźnik optyczny
 */
void hal_indicator_off(void);

/*
 * zmienia stan wskaźnika optycznego na przeciwny
 */
void hal_indicator_toggle(void);

/*
 * sprawdza czy wskaźnik jest włączony
 *
 * @ret		true - wskaźnik włączony
 *		false - wskaźnik wyłączony
 */
bool hal_indicator_is_on(void);

