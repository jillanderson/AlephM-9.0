#pragma once

/** \file **********************************************************************
 *
 * sterowanie zasilaniem
 *
 ******************************************************************************/

/*
 * inicjuje sterowanie zasilaniem
 */
void hal_supply_init(void);

/*
 * włącza zasilanie urządzenia
 */
void hal_supply_on(void);

/*
 * wyłącza zasilanie urządzenia
 */
void hal_supply_off(void);

