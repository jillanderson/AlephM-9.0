#pragma once

/** \file **********************************************************************
 *
 * obsługa przycisku
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjalizuje obsługę przycisku
 */
void hal_key_init(void);

/*
 * sprawdza stan przycisku
 * przycisk w stanie aktywnym (wciśnięty) zwiera pin do masy
 *
 * @ret		true - przycisk wciśnięty
 *		false - przycisk zwolniony
 */
bool hal_key_is_pressed(void);

