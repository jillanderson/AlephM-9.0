#include "../device.h"

#include <avr/builtins.h>
#include <common/finite_state_machine.h>
#include <common/gcc_attributes.h>
#include <common/key_press_type_detector.h>
#include <common/ring_buffer.h>
#include <common/software_timer.h>
#include <stddef.h>
#include <stdint.h>

#include "../../avr/port.h"
#include "../../driver/indicator.h"
#include "../../driver/key.h"
#include "../../driver/speakers.h"
#include "../../driver/supply.h"
#include "../../timebase/timebase.h"

static uint16_t const DEVICE_STARTUP_DELAY_MS = 2500U;

static uint16_t const INDICATOR_ON_TIME_MS = 800U;
static uint16_t const INDICATOR_OFF_TIME_MS = 200U;

static uint8_t const KEY_DEBOUNCING_TIME_MS = 50U;
static uint16_t const KEY_DOUBLEPRESS_TIME_MS = 600U;
static uint16_t const KEY_LONGSTART_TIME_MS = 1000U;
static uint16_t const KEY_LONGREPEAT_TIME_MS = 250U;

static uint16_t const SPEAKERS_LATENCY_TIME_MS = 2500U;

/* musi być potęgą 2^n */
enum {
	EVENT_QUEUE_SIZE = 2U
};

enum device_event {
	DEVICE_NOTHING_EV = FSM_FIRST_USER_EV,
	DEVICE_TIMEOUT_EV,
	DEVICE_SUPPLY_EV
};

static struct device {
	fsm_st fsm;
	ring_buffer_st event_queue;
	uint8_t queue_container[EVENT_QUEUE_SIZE];
	fsm_state_st initial_state;
	fsm_state_st standby_state;
	fsm_state_st working_state;
	software_timer_st startup_timer;
} device = { 0 };

static void initial_handler(fsm_st *me, fsm_msg_st const *msg);
static void standby_handler(fsm_st *me, fsm_msg_st const *msg);
static void working_handler(fsm_st *me, fsm_msg_st const *msg);

static void device_msg_init(void);
static void device_msg_send_event(uint8_t event);
static uint8_t device_msg_read_event(void);

static void key_handler(key_press_event_et event, void *arg);
static void startup_handler(void *arg);

static void tick_time_handler(void *arg);

void device_init(void)
{
	avr_port_initial_settings();

	drv_supply_init();
	drv_speakers_init();
	drv_indicator_init();
	drv_key_init(KEY_DEBOUNCING_TIME_MS, KEY_DOUBLEPRESS_TIME_MS,
			KEY_LONGSTART_TIME_MS, KEY_LONGREPEAT_TIME_MS);

	software_timer_init(&(device.startup_timer));

	timebase_start(tick_time_handler, NULL);

	device_msg_init();

	fsm_state_ctor(&(device.initial_state), initial_handler);
	fsm_state_ctor(&(device.standby_state), standby_handler);
	fsm_state_ctor(&(device.working_state), working_handler);

	fsm_ctor(&(device.fsm), &(device.initial_state));
	fsm_on_start(&(device.fsm));

	__builtin_avr_sei();
}

void device_handle_events(void)
{
	uint8_t event = device_msg_read_event();

	if (DEVICE_NOTHING_EV != event) {
		fsm_on_event(&(device.fsm), &((fsm_msg_st ) { .event = event } ));
	}
}

void initial_handler(fsm_st *me, fsm_msg_st const *msg)
{
	switch (msg->event) {
	case FSM_START_EV:
		software_timer_start(&(device.startup_timer), DEVICE_STARTUP_DELAY_MS, startup_handler, NULL);
		break;

	case FSM_EXIT_EV:
		drv_key_handler(key_handler, NULL);
		break;

	case DEVICE_TIMEOUT_EV:
		fsm_state_transition(me, &(device.standby_state));
		break;

	default:
		;
		break;
	}
}

void standby_handler(fsm_st *me, fsm_msg_st const *msg)
{
	switch (msg->event) {
	case FSM_ENTRY_EV:
		drv_supply_off();
		drv_speakers_disconnect();
		drv_indicator_blink(INDICATOR_ON_TIME_MS, INDICATOR_OFF_TIME_MS);
		break;

	case DEVICE_SUPPLY_EV:
		fsm_state_transition(me, &(device.working_state));
		break;

	default:
		;
		break;
	}
}

void working_handler(fsm_st *me, fsm_msg_st const *msg)
{
	switch (msg->event) {
	case FSM_ENTRY_EV:
		drv_supply_on();
		drv_speakers_connect(SPEAKERS_LATENCY_TIME_MS);
		drv_indicator_on();
		break;

	case DEVICE_SUPPLY_EV:
		fsm_state_transition(me, &(device.standby_state));
		break;

	default:
		;
		break;
	}
}

void device_msg_init(void)
{
	ring_buffer_init(&(device.event_queue), device.queue_container, EVENT_QUEUE_SIZE);
}

void device_msg_send_event(uint8_t event)
{
	ring_buffer_reject_put(&(device.event_queue), event);
}

uint8_t device_msg_read_event(void)
{
	uint8_t event = DEVICE_NOTHING_EV;

	ring_buffer_get(&(device.event_queue), &event);

	return (event);
}

void key_handler(key_press_event_et event, void *arg __gcc_maybe_unused)
{
	switch (event) {
	case KEY_SHORT_PRESSED_EV:
		device_msg_send_event(DEVICE_SUPPLY_EV);
		break;

	default:
		;
		break;
	}
}

void startup_handler(void *arg __gcc_maybe_unused)
{
	device_msg_send_event(DEVICE_TIMEOUT_EV);
}

void tick_time_handler(void *arg __gcc_maybe_unused)
{
	drv_speakers_on_tick_time();
	drv_indicator_on_tick_time();
	drv_key_on_tick_time();
	software_timer_on_tick_time(&(device.startup_timer));
}

