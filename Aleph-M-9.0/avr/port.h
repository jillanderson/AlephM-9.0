#pragma once

/** \file **********************************************************************
 *
 * początkowa inicjalizacja portów wejścia-wyjścia
 *
 ******************************************************************************/

#include <avr/avr_port.h>
#include <avr/io.h>
#include <common/gcc_attributes.h>
#include <stdint.h>

/*
 * włącza podciągnięcie wszystkich wejść do Vcc
 *
 * po resecie kontrolera PORTx, DDRx, PINx są wyzerowane, czyli ustawione jako
 * wejścia bez podciągnięcia do Vcc,
 */
__gcc_static_inline void avr_port_initial_settings(void)
{
	static uint8_t const PULLUP_ON_FOR_ALL = 0xFF;

	avr_port_set_value(&PORTB, PULLUP_ON_FOR_ALL);
	avr_port_set_value(&PORTC, PULLUP_ON_FOR_ALL);
	avr_port_set_value(&PORTD, PULLUP_ON_FOR_ALL);
}

