/** \file **********************************************************************
 *
 * Sterownik wzmacniacza Aleph-M v9.0
 *
 * Copyright (C) 2022r. GNU Public License
 *
 * zzbych2547<at>linux.pl
 *
 * funkcje:
 * 	- sterowanie włączaniem i wyłączaniem
 * 	- opóźnione załączanie głośników przy włączeniu
 * 	- bezzwłoczne odłączanie głośników przy wyłączeniu
 *
 * sterowanie:
 * 	- krótkie naciśnięcie przycisku: zasilanie włącz/wyłącz
 *
 * zasoby sprzętowe:
 * 	- CPU			ATmega8
 * 	- F_CPU			8MHz oscylator wewnętrzny
 * 	- BOD			max 4,5V
 * 	- TIMER2		systemowa podstawa czasu
 *
 * ustawienia kompilatora gcc 7.3.0:
 *
 * opcje avr
 * ---------
 * 	-mrelax			dodaje opcje --mlink-relax do linii
 * 				komend assemblera oraz --relax do linii komend
 * 				linkera)
 * 	-mcall-prologues	rozwija prologi/epilogi funkcji w odrębne
 * 				podprogramy (tu: nieużywana)
 * 	-mstrict-X		użycie rejestru X w sposób proponowany przez
 * 				sprzęt. Używany w adresowaniu pośrednim.
 * 				(tu: nieużywana)
 *
 * opcje gcc
 * ---------
 * 	-std=gnu99		standard c99 i rozszerzenia gnu
 * 	-0s			poziom optymalizacji
 *	-ffunction-sections	odrębne sekcje dla funkcji
 *	-fdata-sections		odrębne sekcje dla danych
 *	-funsigned-char		typ char bez znaku
 *	-funsigned-bitfields	pola bitowe bez znaku
 *	-flto			link time optimization
 *
 * włączone ostrzeżenia gcc
 * ------------------------
 *	-Wall			włącza podstawowe ostrzeżenia
 *	-Wextra			włącza dodatkowe ostrzeżenia
 *	-Werror			traktuje wszystkie ostrzeżenia jak błędy
 *	-Wdouble-promotion
 *	-Wnull-dereference
 *	-Winit-self
 *	-Wmissing-include-dirs
 *	-Wsuggest-attribute=pure
 *	-Wsuggest-attribute=const
 *	-Wsuggest-attribute=noreturn
 *	-Wsuggest-attribute=format
 *	-Wmissing-format-attribute
 *	-Wduplicated-branches
 *	-Wduplicated-cond
 *	-Wfloat-equal
 *	-Wshadow
 *	-Wunsafe-loop-optimizations
 *	-Wpointer-arith
 *	-Wundef
 *	-Wunused-macros
 *	-Wbad-function-cast
 *	-Wcast-qual
 *	-Wcast-align
 *	-Wconversion
 *	-Wlogical-op
 *	-Wstrict-prototypes
 *	-Wmissing-prototypes
 *	-Wmissing-declarations
 *	-Wpacked
 *	-Wpadded
 *	-Wredundant-decls
 *	-Wrestrict
 *	-Winline
 *
 * opcje linkera
 * -------------
 *	-Wl,--gc-sections,--warn-common
 *
 ******************************************************************************/

#include <stdlib.h>

#include "device/device.h"

#include <common/gcc_attributes.h>

__gcc_os_main int main(void)
{
	device_init();

	for (;;) {
		device_handle_events();
	}

	return (EXIT_SUCCESS);
}

