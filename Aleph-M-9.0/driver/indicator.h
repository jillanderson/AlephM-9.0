#pragma once

/** \file **********************************************************************
 *
 * sterowanie wskaźnikiem optycznym w przycisku zasilania
 *
 ******************************************************************************/

#include <stdbool.h>
#include <stdint.h>

typedef enum {
	INDICATOR_OFF = false,
	INDICATOR_ON = true
} indicator_status_et;

/*
 * inicjalizacja wskaźnika
 */
void drv_indicator_init(void);

/*
 * włącza wskaźnik
 */
void drv_indicator_on(void);

/*
 * wyłącza wskaźnik
 */
void drv_indicator_off(void);

/*
 * włącza wskaźnik w tryb błyskania z zadanymi czasami włączenia
 * i wyłączenia wskaźnika
 *
 * @on_time	czas włączenia
 * @off_time	czas wyłączenia
 */
void drv_indicator_blink(uint16_t on_time, uint16_t off_time);

/*
 * testuje czy wskaźnik jest włączony
 *
 * @ret		INDICATO_ON - wskaźnik włączony
 * 		INDICATOR_OFF - wskaźnik wyłączony
 */
indicator_status_et drv_indicator_is_on(void);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void drv_indicator_on_tick_time(void);

