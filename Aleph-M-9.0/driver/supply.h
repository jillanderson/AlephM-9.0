#pragma once

/** \file **********************************************************************
 *
 * sterowanie zasilaniem
 *
 ******************************************************************************/

/*
 * inicjuje sterowanie zasilaniem
 */
void drv_supply_init(void);

/*
 * włącza zasilanie urządzenia
 */
void drv_supply_on(void);

/*
 * wyłącza zasilanie urządzenia
 */
void drv_supply_off(void);

