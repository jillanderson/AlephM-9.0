#pragma once

/** \file **********************************************************************
 *
 * sterowanie podłączaniem zespołów głośnikowych do końcówki mocy
 *
 ******************************************************************************/

#include <stdint.h>

/*
 * inicjalizacja sterowania głośnikami
 */
void drv_speakers_init(void);

/*
 * przyłącza głośniki z zadaną zwłoką czasową
 *
 * @connection_latency_ms	czas opóźnienia
 */
void drv_speakers_connect(uint16_t connection_latency_ms);

/*
 * odłącza głośniki bezzwłocznie
 */
void drv_speakers_disconnect(void);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void drv_speakers_on_tick_time(void);

