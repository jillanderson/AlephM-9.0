#include "../../hal/speakers.h"
#include "../speakers.h"

#include <stdint.h>

static struct {
	volatile uint16_t latency_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_COUNTING
	} counter_state;
} speakers = { 0 };

void drv_speakers_init(void)
{
	hal_speakers_init();
}

void drv_speakers_connect(uint16_t connection_latency_ms)
{
	if ((false == hal_speakers_is_on()) && (COUNTER_STOPPED == speakers.counter_state)) {
		speakers.latency_counter = connection_latency_ms;
		speakers.counter_state = COUNTER_COUNTING;
	}
}

void drv_speakers_disconnect(void)
{
	hal_speakers_off();
	speakers.counter_state = COUNTER_STOPPED;
}

void drv_speakers_on_tick_time(void)
{
	if (COUNTER_COUNTING == speakers.counter_state) {

		if (0U < speakers.latency_counter) {
			speakers.latency_counter--;
		}

		if (0U == speakers.latency_counter) {
			hal_speakers_on();
			speakers.counter_state = COUNTER_STOPPED;
		}
	}
}

