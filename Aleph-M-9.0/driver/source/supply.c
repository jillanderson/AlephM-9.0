#include "../../hal/supply.h"
#include "../supply.h"

void drv_supply_init(void)
{
	hal_supply_init();
}

void drv_supply_on(void)
{
	hal_supply_on();
}

void drv_supply_off(void)
{
	hal_supply_off();
}

